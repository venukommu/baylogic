
<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Contact |bayLogic Technologies | Contact bayLogic</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">
	
	 <!-- alertyfy js -->
  	<link rel="stylesheet" href="js/contact-page/alertifyjs/css/alertify.css">
	
	<link rel="stylesheet" href="js/contact-page/alertifyjs/css/themes/bootstrap.min.css"/>
	
	<script src="js/contact-page/alertifyjs/alertify.js"></script>
	
	
	
	<!-- Contact Us Sidebar Button -->
	<!-- <script type="text/javascript">var _cmo = {form: '53c8f22b525eef00020063d0', text: 'Contact Us', align: 'right', valign: 'middle', lang: 'en', background_color: '#B51705'}; (function() {var cms = document.createElement('script'); cms.type = 'text/javascript'; cms.async = true; cms.src = ('https:' == document.location.protocol ? 'https://d1uwd25yvxu96k.cloudfront.net' : 'http://static.contactme.com') + '/widgets/tab/v1/tab.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cms, s);})();</script> -->
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_contact_info">
        
        <div class="container">
        
            <ul class="tci_list_left">
                <li><a href="index.html">HOME</a></li>
                <li>|</li>
				<li><a href="portfolio.html">PORTFOLIO</a></li>
            <!--     <li>|</li>
                <li><a href="#">Buy Now!</a></li> -->
            </ul>
        
            <ul class="tci_list">
            
                <li class="empty"><i class="icon-phone-sign"></i> +91 0891-2710598 </li>
                <li class="empty"><a href="mailto:ravip@baylogictech.com"><i class="icon-envelope"></i> ravip@baylogictech.com</a></li>
                <!-- <li class="empty"><a href="#"><i class="icon-skype"></i> rpasupul</a></li> -->
				<li> Follow Us &nbsp; &nbsp; </li>
                <li><a href="https://www.facebook.com/bayLogicTechnologies"><i class="icon-facebook"></i></a></li>
                <li><a href="https://twitter.com/bayLogictech"><i class="icon-twitter"></i></a></li>
                <li><a href="https://plus.google.com/u/0/106134539158163721625"><i class="icon-google-plus"></i></a></li>
                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                <!-- <li><a href="#"><i class="icon-flickr"></i></a></li> -->
                <li><a href="#"><i class="icon-youtube"></i></a></li>
                <!-- <li><a href="#"><i class="icon-rss"></i></a></li> -->
                 
            </ul>
            
        </div>
        
    </div><!-- end top contact info -->
            
 	</div>
    
	</div>
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="one_fourth"><a href="index.html"><img src="images/logo.png" width="216" height="70"/></a></div>
		
        <!-- Menu -->
        <div class="three_fourth last">
           
           <nav id="access" class="access" role="navigation">
           
                        <div id="menu" class="menu">
                
                <ul id="tiny">
                
                    <li><a href="index.html">Home</a></li>  
                    
                    <li><a>About Us </a>
                    
                    	<ul>
                            <li><a href="corporate-overview.html">Corporate Overview</a></li>
                            <li><a href="our-core-values.html">Our Core Values</a></li>
                            <li><a href="management-team.html">Management Team</a></li>
                            <li><a href="technical-team.html">Technical Team</a></li>
                        </ul>
                    
                    </li>
                    
                    <li><a>Services</a>
                    
                        <ul>
                            <li><a href="services-overview.html">Services Overview</a></li>
                            <li><a href="j2ee-applications.html">J2EE Applications</a></li>
							<li><a href="mobile-applications.html">Mobile Applications</a></li>							
                            <li><a href="4gl-informix-solutions.html">Informix solutions</a></li>
                            <li><a href="web-development.html">Web Development</a></li>
                            <li><a href="business-intelligence-tools.html">BI Tools</a></li>

                          <!--   <li><a href="left-sidebar.html">Left Sidebar Page</a></li>
                            <li><a href="right-sidebar.html">Right Sidebar Page</a></li>
                            <li><a href="left-nav.html">Left Navigation</a></li>
                            <li><a href="right-nav.html">Right Navigation</a></li>
                            <li><a href="404.html">404 Error Page</a></li> -->
                        </ul>
                        
                    </li>
                    
                    <li><a>Products</a>
                    
                        <ul>
                            <li><a href="tms-traffic-management-system.html">TMS for Enterprises</a></li>
                            <li><a href="msds-material-safety-data-sheet.html">MSDS Manager</a></li>
                            <li><a href="phse-private-home-schooling-emulator.html">PHSE For Schools</a></li>
                            <li><a href="ehrms-enterprise-human-resource-management-system.html">EHRMS - eProfile</a></li>
                            <li><a href="room-request.html">Room Request</a></li>
                            <li><a href="online-recruiter.html">Online Recruiter</a></li>
                           <!-- <li><a href="tabs.html">Tabs</a></li>
                            <li><a href="#">4 Diffrent Layouts</a></li>                         
                            <li><a href="#">Custom BGs &amp; Colors</a></li>
                            <li><a href="#">PSD Files Included</a></li>                             
                            <li><a href="#">Clean &amp; Valid Code</a></li>
                            <li><a href="#">Useful Typo Elements</a></li>
                            <li><a href="#">Cross Browser Check</a></li> -->  
                        </ul>
                        
                    </li>
  
                    <li><a href="guests-comments.html">Comments <!-- <i class="icon-angle-down"></i> --></a>
                    
                        <!-- <ul>
                            <li><a href="portfolio-one.html">Single Image</a></li>
                            <li><a href="portfolio-two.html">2 Columns</a></li>
                            <li><a href="portfolio-three.html">3 Columns</a></li>
                            <li><a href="portfolio-four.html">4 Columns</a></li>
                            <li><a href="portfolio-five.html">Portfolio + Sidebar</a></li>
                            <li><a href="portfolio-six.html">Portfolio Fancy</a></li>
                        </ul> -->
                    </li>
                    
                    <li><a href="contact.php" class="active">Contact<!-- <i class="icon-angle-down"></i> --></a>
                    
                        <!-- <ul>
                            <li><a href="blog.html">with Large Image</a></li>
                            <li><a href="blog-2.html">with Small Image</a></li>
                            <li><a href="blog-post.html">Single Post</a></li>
                        </ul> -->
                    </li>
                    
                    <!-- <li class="last"><a href="contact.html">Contact</a></li> -->
                    
                </ul>
                
            </div>
            
        </nav><!-- end nav menu -->
      
        </div>
        
        
		</div>
		
	</div>
    
	</div>
    
</header><!-- end header -->

<div class="clearfix"></div>

<div class="page_title">

	<div class="container">
		<div class="title"><h1>Contact</h1></div>
        <div class="pagenation">&nbsp;<a href="index.html">Home</a> <i>/</i>Contact</div>
	</div>
</div><!-- end page title --> 


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">
        	
    <div class="one_half">
        		
		<h3><i>Contact Us</i></h3>
		<p align="justify">Feel free to talk to our online representative at any time using our Live Chat system or you can use the below form to contact us.</p>
		<br />
		<p align="justify">Please be patient while waiting for response. <strong>Contact for  General Inquiries: 0891-2710598</strong></p>

		<br />			
			<link rel="stylesheet" href="css/style.css" />
			
			<!-- Contact page validation using jQuery -->
			<script src="js/jquery/jquery.min.js"></script>
			<link rel="stylesheet" href="css/style.css" />
			<script src="js/contact-page/contact.js"></script>
			 
			<!-- Recaptcha for contact Page --> 
			<script src='js/recaptcha/api.js'></script>
		</head>
		
		<body>
		
		
		<form id="myform" >
			<div class ="errormsg">
				<p id="returnmessage" class="errormsg"></p>
			</div>
			<fieldset>			
				<label for="name" class="blocklabel">Name:*</label>
				<p class="" >
					<input name="yourname" class="input_bg" type="text" id="name" value=''/>
				</p> 
				
				<label for="email" class="blocklabel">Email:*</label>
				<p class="" >
					<input name="email" class="input_bg" type="text" id="email"  type="email" value='' />
				</p>
				
				<label for="phone" class="blocklabel">Mobile:*</label>
				<p class="" >
					<input name="phone" class="input_bg" type="text" id="phone" value=""/>
				</p>
				
				<label for="message" rows="7" cols="20" maxlength="500" class="blocklabel">Message:*</label>
				<p class="" >
					<textarea name="message" class="textarea_bg" id="message" cols="20" rows="7" ></textarea>
				</p>  
				
				<p>
					<div class="clearfix"></div> 
					<input  type="checkbox" name="checkbox" id="checkbox">Copy to myself
				</p>
				<br>
				
				<div class="g-recaptcha" data-sitekey="6LfhCicUAAAAAFq26j_oAC_MqGoDK9KREvFRiuCo"></div>
				<br>
				<p>
					<div class="clearfix"></div>
					<input name="Send" type="Button"  id="submit" value="SUBMIT"/>
				</p>
			</fieldset>
			
		</form> 
		<div id="loading-div-background" class="loading-div-background" style="padding-top: 15%;display:none;">
				<div class="innerProgress" id="fdprogress"><img src="images/gears.gif"></div>
		</div>
    </div>
	</body>
               
    <div class="one_half last">
    
        <div class="address-info">
            <h3><i>Get In Touch</i></h3>
                <ul>
                <li>
                <strong>bayLogic Technologies</strong><br />
                15-6-6, Saileshwar Sen Marg,<br /> 
				Visakhapatnam, AP, India - 530002<br />
                Telephone: 91 0891-2710598<br />
                Phone: 91 94400-43517<br />
                E-mail: <a href="mailto:mail@companyname.com">ravip@baylogictech.com</a><br />
                Website: <a href="index.html">www.baylogictech.com</a>
                </li>
            </ul>
        </div>

         <h3><i>Find the Address</i></h3>
            <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3800.744880186998!2d83.31382599999999!3d17.709508!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x12c3329a207ffca0!2sBaylogic+Technologies!5e0!3m2!1sen!2sin!4v1405681706633&amp;output=embed"></iframe><br /><small><a href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3800.744880186998!2d83.31382599999999!3d17.709508!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x12c3329a207ffca0!2sBaylogic+Technologies!5e0!3m2!1sen!2sin!4v1405681706633"></a></small>        
    </div>
            
</div>

</div><!-- end content area -->


<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<div class="footer">

	<div class="arrow_02"></div>
	
    <div class="clearfix mar_top5"></div>
	
    <div class="container">
    
   		<div class="one_fourth">
            
            <div class="footer_logo"><img src="images/logos/blt-logo-venu-new.png" alt="bayLogic Technologies" /></div><!-- end footer logo -->
            
            <ul class="contact_address">
                <li><i class="icon-map-marker icon-large"></i>&nbsp; 15-6-6, Saileshwar Sen Marg,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Visakhapatnam, Andhra Pradesh,</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; India - 530002</li>
                <li><i class="icon-phone"></i>&nbsp; +91 0891-2710598 </li>
                <li><i class="icon-print"></i>&nbsp; - 0891 - 271 - 0598</li>
                <li><img src="images/footer-wmap.png" alt="" /></li>
            </ul>
            
        </div><!-- end address section -->
        
        <div class="one_fourth">
        	
            <h2>Useful <i>Links</i></h2>
            
            <ul class="list">
                <li><a href="Java-development-and-maintenance.html"><i class="icon-angle-right"></i> Java Development  &amp; Maintenance</a></li>
                <li><a href="jaspersoft-development.html"><i class="icon-angle-right"></i> JasperSoft Development</a></li>
                <li><a href="cognos-development-services.html"><i class="icon-angle-right"></i> Cognos Development Services</a></li>
                <li><a href="4GL-development-maintenance.html"><i class="icon-angle-right"></i> 4GL Development &amp; Maintenance</a></li>
                <li><a href="mobile-applications-building.html"><i class="icon-angle-right"></i> Mobile Applications Building</a></li>
                <li><a href="dotCMS-development.html"><i class="icon-angle-right"></i> dotCMS Development</a></li>
				<li><a href="joomla-development.html"><i class="icon-angle-right"></i> Joomla Development</a></li>
                <li><a href="web-applications-development.html"><i class="icon-angle-right"></i> Web Applications Development</a></li>
				<li><a href="business-intelligence-tools.html"><i class="icon-angle-right"></i> Business Intelligence Tools</a></li>
				
            </ul>
            
        </div><!-- end useful links -->
         
        <div class="one_fourth">
        	
            <div class="twitter_feed">
            
            	<h2>Latest <i>Tweets</i></h2>
				<a class="twitter-timeline" href="https://twitter.com/bayLogictech" data-widget-id="466536030112202752">Tweets by @bayLogictech</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

            </div>
            
        </div><!-- end tweets -->
        
		<div class="one_fourth last">
		<h2>Find us on Facebook</h2>
			<div id="fb-root"></div>
            <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            </script>
			<div class="fb-like-box" data-href="https://www.facebook.com/bayLogicTechnologies" data-width="250" data-height="300" data-show-faces="true" data-stream="true" data-show-border="true" data-header="true"></div>
			<div class="clear"></div>
		</div>
		
        <!--<div class="one_fourth last">
        	
            <h2>Flickr <i>Photos</i></h2>
            
        	<div id="flickr_badge_wrapper">
            	<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=h&amp;source=user&amp;user=93382411%40N07"></script>     
            </div>
            
        </div>--><!-- end flickr -->
        
        
    </div>
	
    <div class="clearfix mar_top5"></div>
    
</div>


<div class="copyright_info">

    <div class="container">
    
        <div class="one_half">
			<b>All Copyrights Reserved © <script>document.write("1999 - "+new Date().getFullYear());</script> <a  title="Designed and developed by" alt="Designed and developed by" href="http://www.baylogictech.com"> <strong>&nbsp; bayLogic Technologies</strong> </a></b>
            <!-- <b>All Copyrights Reserved © 2014 <a href="http://www.baylogictech.com"> www.baylogictech.com </a> </b> -->
            
        </div>
    
    	<div class="one_half last">
     		
            <ul class="footer_social_links">
                <li><a href="https://www.facebook.com/bayLogicTechnologies"><i class="icon-facebook"></i></a></li>
                <li><a href="https://twitter.com/bayLogictech"><i class="icon-twitter"></i></a></li>
                <li><a href="https://plus.google.com/u/0/106134539158163721625"><i class="icon-google-plus"></i></a></li>
                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                <li><a href="#"><i class="icon-youtube"></i></a></li>				
                <!-- <li><a href="#"><i class="icon-skype"></i></a></li>
                <li><a href="#"><i class="icon-flickr"></i></a></li>
                <li><a href="#"><i class="icon-html5"></i></a></li>
                <li><a href="#"><i class="icon-rss"></i></a></li> -->
            </ul>
                
    	</div>
    
    </div>
    
</div><!-- end copyright info -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>

<!-- js script for twitter feeds -->
<!--<script type="text/javascript" src="js/twitterfeed/twtrfeed.js"></script>-->
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/592fe96bb3d02e11ecc67bcb/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0); 
	})();
	</script>
	<!--End of Tawk.to Script-->

</body>
</html>
