<!DOCTYPE HTML>
<html>
<head>
<style>
.error{
	color: #FF0000;
   }
</style>
<script>
 //The validation of the form by using JavaScript.
 function submitForm(f){
	 if(!contactValidation(f))
	 {
		 return;
	 }
	 var xmlHttp=new XMLHttpRequest();
        var url="rammail.php";
        var params="&name="+f.name.value;
        params+="&email="+f.email.value;
        params+="&phone="+f.phone.value;
		params+="&comment="+f.message.value;
		        
         xmlHttp.onreadystatechange=function()
        {
                if(xmlHttp.readyState==4)
                {
                        if(xmlHttp.status==200)
                        {
							
                            alert(xmlHttp.responseText);
							f.name.value='';
                            f.email.value='';
                            f.phone.value='';
						    f.message.value='';
							document.getElementById('ftprogress').innerHTML='';
						}
                                
                } else { 
				document.getElementById('ftprogress').innerHTML='<img src="progressbar.gif">';
				}
        }
				 xmlHttp.open("POST", url, true);
        xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlHttp.send(params);
}
	 
//Fields validation
function contactValidation(f) {
	
	 var nameCond = /^[a-zA-Z ]+$/;	  
     var mailFormat = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     var chkphn = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
	 if(f.name.value=="" || f.name.value == null) {
		 alert("Please enter the name!");
		 document.getElementById("name").focus();
		 return false;
	 }
	 else if(!f.name.value.match(nameCond)){
		 alert("OH!.. Only letters and white spaces are allowed");
		 document.getElementById("name").focus();
		 return false;
	 } else if(f.phone.value == "" || f.phone.value == null) {
		 alert("Please enter the Phone number!");
		 document.getElementById("phone").focus();
		 return false;
	 } 
	 else if(!f.phone.value.match(chkphn)){
		 alert("Please enter valid phone number!");
		 document.getElementById("phone").focus();
		 return false;
	 } else if(f.email.value == "" || f.email.value == null){
		 alert("Please enter the Email address!");
		 document.getElementById("email").focus();
		 return false;
	 } else if(!f.email.value.match(mailFormat)){
		 alert("Please enter valid email address!");
		 document.getElementById("email").focus();
		 return false;
	 }else if(f.message.value=="")
        {
                alert("Please Enter message");
                return false;
        }
		else
		{
			return true;
		}
}
</script> 
</head> 
<body>
	<h3><i>Contact Form</i></h3>
		<form id="myform" method = "post">
			<div class ="errormsg">
				<p id="returnmessage" class="errormsg"></p>
			</div>
			<fieldset>			
				<label for="name" class="blocklabel">Name:*</label>
				<p class="" >
					<input name="yourname" class="input_bg" type="text" id="name" value=''/>
				</p> 
				
				<label for="email" class="blocklabel">Email:*</label>
				<p class="" >
					<input name="email" class="input_bg" type="text" id="email"  type="email" value='' />
				</p>
				
				<label for="phone" class="blocklabel">Mobile:*</label>
				<p class="" >
					<input name="phone" class="input_bg" type="text" id="phone" value=""/>
				</p>
				
				<label for="message" rows="7" cols="20" maxlength="500" class="blocklabel">Message:*</label>
				<p class="" >
					<textarea name="message" class="textarea_bg" id="message" cols="20" rows="7" ></textarea>
				</p>  
				
				<p>
					<div class="clearfix"></div> 
					<input  type="checkbox" name="checkbox" id="checkbox">Copy to myself
				</p>
				<br>
				
				<div class="g-recaptcha" data-sitekey="6LfhCicUAAAAAFq26j_oAC_MqGoDK9KREvFRiuCo"></div>
				<br>
				<p>
					<div class="clearfix"></div>
					<input type = "button" value = "submit" onClick = "submitForm(this.form)">
							<div class="innerProgress" id="ftprogress"></div>
				</p>
			</fieldset>
		</form> 
</body>