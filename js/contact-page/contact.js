$(document).ready(function () {
	$("#submit").click(function () {
		$('html, body').animate({
			scrollTop: $("p").offset().top
		}, 5);
		var name = $("#name").val();
		var email = $("#email").val();
		var message = $("#message").val();
		var contact = $("#phone").val();
		var checkbox = $("#checkbox").prop("checked");
		var recaptcha = $("#recaptcha").prop("checked");
		$("#returnmessage").empty();
		if (validate()) {
			$("#loading-div-background").show();
			$.ajax({
				type: "POST",
				url: "sendmail.php",
				data: "action=sendMailToContact&name=" + name + "&email=" + email + "&message=" + message + "&contact=" + contact + "&checkbox=" + checkbox + recaptcha + "&recaptcha=",
				async: true,
				success: function (data) {
					alertify.set('notifier', 'position', 'bottom-left');
					alertify.success(data);
					if (data != "") {
						$("#myform")[0].reset();
					}
					$("#loading-div-background").hide();
				}
			});
		}
	});
});

function validate() {
	var name = $("#name").val();
	var email = $("#email").val();
	var message = $("#message").val();
	var contact = $("#phone").val();

	var name_regex = /^[a-zA-Z ]+$/;
	var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	var phone_regex = /^\d{10}$/;
	if (name == '' && email == '' && contact == '' && message == '') {
		$("#returnmessage").html("Please Fill Required Fields");
	}
	else if (!name.match(name_regex) || name.length == 0) {
		$("#name").focus();
		$("#returnmessage").html("Only letters and white space allowed!");
		return false;
	}
	else if (!email.match(email_regex) || email.length == 0) {
		$("#returnmessage").html("Please enter a valid email address");
		$("#email").focus();
		return false;
	}
	else if (!contact.match(phone_regex) || contact.length == 0) {
		$("#returnmessage").html("Please enter a valid phone number");
		$("#phone").focus();
		return false;
	}
	else if (message.length == 0) {
		$("#returnmessage").html("Please enter message");
		$("#message").focus();
		return false;
	}
	else {
		return true;
	}
}
