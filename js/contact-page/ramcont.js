$(document).ready(function() {
	$("#submit").click(function() {
		var name = $("#name").val();
		var email = $("#email").val();
		var message = $("#message").val();
		var contact = $("#phone").val();
		var checkbox = $("#checkbox").prop("checked");
		if(validate()){
			$("#loading-div-background").show();
			$.ajax({
				type : "POST",	
				url : "sendmail.php",
				data : "action=sendMailToContact&name=" + name + "&email=" + email+ "&message=" + message+"&contact="+contact + "&checkbox="+checkbox,
				async: true,
				success : function(data) {
					alertify.success(data);
					if (data != "") {
						$("#myform")[0].reset();
					}
					$("#loading-div-background").hide();	
				}
			});
		}
	});
});

function validate() {
	var name = $("#name").val();
	var email = $("#email").val();
	var message = $("#message").val();
	var contact = $("#phone").val();

	var name_regex = /^[a-zA-Z ]+$/;
	var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	var phone_regex= /^\d{10}$/;
	if (name == '' && email == '' && contact == '' && message == '') {
		alert("Please Fill Required Fields");
	} 
	if (name == '' || name == null) {
		alert("Please enter the name");
		$("#name").focus();
		return false;
	}
	else if (!name.match(name_regex)) {
		alert("Only letters and white space allowed!");
		$("#name").focus();
		return false;
	}
	else if (email == '' || email == null) {
		alert("Please enter the Email address!");
		$("#email").focus();
		return false;
	}
	else if(!email.match(email_regex)) {
		alert("Please enter a valid email address");
		$("#email").focus();
		return false;
	}
	else if(!contact.match(phone_regex) || contact.length == 0) {
		alert("Please enter a valid phone number");
		$("#phone").focus();
		return false;
	}
	else if(message.length == 0) {
		alert("Please enter message");
		$("#message").focus();
		return false;
	}
	else {
		return true;
	}
}
