<?php
if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "ravip@baylogictech.com";	
    $email_subject = "[www.baylogictech.com] Contact from bayLogictech.com";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
 
    /* validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['website']) ||
        !isset($_POST['message'])
        ) 
         {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    } */
 
    $name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $website = $_POST['website']; // not required
    $message = $_POST['message']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    /*honeyspot*/
    $honeypot = FALSE;
    if (!empty($_REQUEST['contact_me_by_fax_only']) && (bool) $_REQUEST['contact_me_by_fax_only'] == TRUE)  {
        $honeypot = TRUE;
        $error_message .= "Sorry bot";
       // log_spambot($_REQUEST);
        //echo  $error_message;
        # treat as spambot
    } else {
        # process as normal
    }
    if (!empty($_REQUEST['password'])) {
      $honeypot = TRUE;
      $error_message .= "Sorry bot";
    }
   
  //reCAPTCHA validation
  //Site-key - 6Lf6XL8bAAAAAAXwu_3Ct9QAI_BPa8UlBTUm0R5p
  //Secret key -6Lf6XL8bAAAAACemPy_12i5GrV6mC__jqg0fMbjJ
  if(!empty($_POST['g-recaptcha-response'])  )
  {
      //$secret = 'GOOGLE_CAPTACH_SECRET_KEY';            
      $secret = '6Lf6XL8bAAAAACemPy_12i5GrV6mC__jqg0fMbjJ';
      $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
      $responseData = json_decode($verifyResponse);
      if($responseData->success){
         // $messagecaptcha = "g-recaptcha verified successfully";
      }
      else{
        $error_message = "Some error in verifying g-recaptcha";
      }
     echo $error_message;
  }

  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
 
  /*if(!preg_match($string_exp, $website)) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }*/
  if(strlen($message) < 2) {
    $error_message .= 'The Message you entered do not appear to be valid.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message .= "Your name: ".clean_string($name)."\n";
    $email_message .= "Message: ".clean_string($message)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Website: ".clean_string($website)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
header('Location: thank-you.html');
exit; 
?>
 
<!-- include your own success html here -->
 

<?php

}
?>